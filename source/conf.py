#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, time
sys.path.append(os.path.abspath('matplotlib'))
sys.path.append(os.path.abspath('numpy'))
sys.path.append(os.path.abspath('py2'))
sys.path.append(os.path.abspath('py3'))
sys.path.append(os.path.abspath('sympy'))

extensions = [
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.imgmath',
    'sphinx.ext.ifconfig'
]

mathjax_path = 'https://cdn.mathjax.org/mathjax/latest/MathJax.js'

applehelp_bundle_name = 'Pythonista'
applehelp_icon = '_static/pythonista_icon.png'
applehelp_remote_url = 'http://omz-software.com/pythonista/docs/'
applehelp_index_anchors = True
applehelp_title = 'Pythonista Documentation'

templates_path = ['_templates']
source_suffix = '.txt'
master_doc = 'contents'
project = 'Pythonista'
version = '3'
release = '3.0'
today = ''
today_fmt = '%B %d, %Y'
exclude_patterns = [
    'venv/*'
    #'numpy/*',
    #'sympy/*'
    #'py2/*'
]

#show_authors = False
pygments_style = 'sphinx'
#modindex_common_prefix = []
#keep_warnings = False
#html_theme = 'alabaster'
#html_theme_options = {}
#html_theme_path = []
html_title = 'Pythonista 3'
html_short_title = 'Pythonista 3'
html_static_path = ['_static']
#html_extra_path = []
html_last_updated_fmt = '%b %d, %Y'
#html_additional_pages = {}

html_use_index = True
html_split_index = False
html_show_sphinx = False
html_show_copyright = False
html_use_opensearch = 'http://omz-software.com/pythonista/docs/'
intersphinx_mapping = {
    #'pythonista': ('http://omz-software.com/pythonista/docs/', 'Documentation.inv'),
    'py2': ('http://omz-software.com/pythonista/docs/', 'py2/objects.inv'),
    'py3': ('http://omz-software.com/pythonista/docs/', 'py3/objects.inv'),
    'numpy': ('http://omz-software.com/pythonista/numpy/', 'numpy/objects.inv'),
    'matplotlib': ('http://omz-software.com/pythonista/matplotlib/', 'matplotlib/objects.inv'),
    'sympy': ('http://omz-software.com/pythonista/sympy/', 'sympy/objects.inv')
}
